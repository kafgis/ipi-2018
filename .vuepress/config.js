module.exports = {
    base: '/ipi-2018/',
    dest: 'public',
    title: 'Информационная система',
    description: "ИПИ - информационная поддержка жизненного цикла изделий",
    components: [
        {
            path: '../components/TestContainer.vue',
            name: 'test-container',
        }
    ],
    themeConfig:{
        nav: [
            {
                text: 'Лекции', 
                link: '/lections/',
            },
            {
                text: 'Практика',
                link: '/practice/',
            },
            {
                text: 'Тестирование',
                link: '/tests/',
            },
        ],
        sidebar: {
            '/lections/': [
                {
                    title: 'Лекции',
                    collapsable: false,
                    children: [
                        '/lections/1',
                        '/lections/2',
                        '/lections/3',
                        '/lections/4',
                        '/lections/5',
                        '/lections/6',
                        '/lections/7',
                        '/lections/8',
                        '/lections/9',
                        '/lections/10',
                        '/lections/11',
                        '/lections/12',
                        '/lections/13',
                        '/lections/14',
                        '/lections/15',
                        '/lections/16',
                        '/lections/17',
                    ],
                },
            ],
            '/practice/': [
                {
                    title: 'Практика',
                    collapsable: false,
                    children: [
                        '/practice/1',
                        '/practice/2',
                        '/practice/3',
                    ],
                },
            ],
            '/tests/': [
                {
                    title: 'Тестирование',
                    collapsable: false,
                    children: [
                        '/tests/1',
                        '/tests/2',
                        '/tests/3',
                        '/tests/4',
                        '/tests/5',
                        '/tests/6',
                        '/tests/7',
                        '/tests/8',
                        '/tests/9',
                        '/tests/10',
                        '/tests/11',
                        '/tests/12',
                        '/tests/13',
                        '/tests/14',
                        '/tests/15',
                        '/tests/16',
                        '/tests/17',
                    ],
                },
            ],
        },
    },
}
